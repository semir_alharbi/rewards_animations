import 'package:flutter/material.dart';

extension BuildContextExtension on BuildContext {
  double get mqHeight => MediaQuery.of(this).size.height;

  double get mqWidth => MediaQuery.of(this).size.width;
}
