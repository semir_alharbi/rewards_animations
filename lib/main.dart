import 'package:flutter/material.dart';
import 'package:rewards_animations/rewards_animations/models/reward_animation_model.dart';
import 'package:rewards_animations/rewards_animations/view/rewards_animations.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        colorScheme: ColorScheme.fromSeed(seedColor: Colors.deepPurple),
        useMaterial3: true,
      ),
      home: const HomePage(),
    );
  }
}

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  List<RewardAnimationModel> _rewards = [];
  @override
  void initState() {
    super.initState();
    _rewards = [
      const RewardAnimationModel(
        assetPath: 'assets/reward_icon.webp',
        ribbonText: 'ribbonText1',
        subtitle: 'reward1',
      ),
      const RewardAnimationModel(
        assetPath: 'assets/reward_icon.webp',
        ribbonText: 'ribbonText2',
        subtitle: 'reward2',
      ),
      const RewardAnimationModel(
        assetPath: 'assets/reward_icon.webp',
        ribbonText: 'ribbonText3',
        subtitle: 'reward3',
      ),
    ];
  }
  @override
  Widget build(BuildContext context) {

    return Scaffold(
      backgroundColor: Colors.purple,
      body: SafeArea(
        child: RewardsAnimation(
          rewardAnimationModels: _rewards,
          onButtonPressed: () {},
          buttonText: 'buttonText',
        ),
      ),
    );
  }
}
