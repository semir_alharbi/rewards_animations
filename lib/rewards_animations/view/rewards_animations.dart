import 'dart:async';

import 'package:flutter/material.dart';
import 'package:rewards_animations/rewards_animations/models/reward_animation_model.dart';
import 'package:rewards_animations/rewards_animations/widgets/animated_reward.dart';
import 'package:rewards_animations/rewards_animations/widgets/custom_cta.dart';
import 'package:rewards_animations/rewards_animations/widgets/text_ribbon.dart';
import 'package:rewards_animations/utils/context_extensions.dart';

class RewardsAnimation extends StatefulWidget {
  const RewardsAnimation({
    super.key,
    required this.rewardAnimationModels,
    required this.onButtonPressed,
    required this.buttonText,
  }) : assert(
          rewardAnimationModels.length <= 11,
          'There are more models than the allowed value. The maximum value is 11...',
        );

  /// Model of rewards parsed to the widget. Widget depends on models length to position the rewards
  /// Max amount of models = 11
  final List<RewardAnimationModel> rewardAnimationModels;
  final VoidCallback onButtonPressed;
  final String buttonText;

  @override
  RewardsAnimationState createState() => RewardsAnimationState();
}

class RewardsAnimationState extends State<RewardsAnimation> with TickerProviderStateMixin {
  List<AnimationController> _rewardsControllers = [];
  final List<Animation<double>> _fadeInAnimations = [];
  final List<Animation<double>> _moveAnimations = [];
  final List<Animation<double>> _rotationAnimations = [];
  final List<Animation<double>> _rewardMoveVertical = [];
  final List<AnimationStatusListener?> _listeners = [];

  bool _isFirstPhase = true;

  late AnimationController _rewardSplitController;
  late AnimationController _ribbonController;
  late AnimationController _buttonController;

  late Animation<double> _ribbonFadeInAnimation;
  late Animation<double> _ribbonFadeOutAnimation;

  late Animation<double> _buttonFadeInAnimation;

  String _ribbonText = '';

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((_) async {
      await _setControllers();
      _setAnimations();
      await _startAnimations(0);
    });
  }

  Future<void> _startAnimations(int index) async {
    if (index >= _rewardsControllers.length || _rewardsControllers.isEmpty) return;
    setState(() {
      _ribbonText = widget.rewardAnimationModels[index].ribbonText;
    });
    final AnimationController currentController = _rewardsControllers[index];
    await currentController.forward().then((_) {
      if (index == _rewardsControllers.length - 1) {
        _rewardSplitController.forward();
        _buttonController.forward();
        setState(() {
          _isFirstPhase = false;
        });
      }
      if (index < _rewardsControllers.length - 1) {
        _ribbonController
          ..reset()
          ..forward();
      }
      _startAnimations(index + 1);
    });
  }

  Widget _buildAnimatedBuilder() {
    final List<Widget> animatedBuilders = [];
    for (int i = 0; i < widget.rewardAnimationModels.length; i++) {
      animatedBuilders.add(
        AnimatedBuilder(
          animation: Listenable.merge([_rewardsControllers[i], _rewardSplitController]),
          builder: (context, child) => Positioned(
            bottom: context.mqHeight * _moveAnimations[i].value,
            right: _rewardMoveVertical[i].value,
            child: Transform.rotate(
              angle: _isFirstPhase ? _rotationAnimations[i].value * 6.3 : 0,
              child: AnimatedReward(
                animationFadeIn: _fadeInAnimations[i],
                rewardAsset: widget.rewardAnimationModels[i].assetPath,
                rewardTitle: widget.rewardAnimationModels[i].subtitle,
              ),
            ),
          ),
        ),
      );
    }
    return Stack(
      children: animatedBuilders,
    );
  }

  Future<void> _setControllers() async {
    _rewardsControllers = List.generate(
      widget.rewardAnimationModels.length,
      (index) => AnimationController(
        vsync: this,
        duration: const Duration(seconds: 1, milliseconds: 500),
      ),
    );

    _rewardSplitController = AnimationController(
      vsync: this,
      duration: const Duration(milliseconds: 600),
    );

    _ribbonController = AnimationController(
      vsync: this,
      duration: const Duration(milliseconds: 500),
    );
    _buttonController = AnimationController(
      vsync: this,
      duration: const Duration(milliseconds: 500),
    );
  }

  void _setAnimations() {
    final int rewardsCount = widget.rewardAnimationModels.length;
    final double initialVerticalEnd = rewardsCount % 3 == 0 ? context.mqWidth * 0.62 : context.mqWidth * 0.74;
    double verticalEnd = initialVerticalEnd;
    double horizontalEnd = 0.5;
    int controllerCount = 0;

    final List<double> verticalEndValues = [
      context.mqWidth * 0.62,
      context.mqWidth * 0.38,
      context.mqWidth * 0.14,
    ];

    for (final controller in _rewardsControllers) {
      controllerCount += 1;

      if (verticalEnd > context.mqWidth * 0.74 ||
          verticalEnd <= context.mqWidth * 0.02 ||
          (controllerCount % 4 == 0 && rewardsCount % 3 != 0)) {
        verticalEnd = initialVerticalEnd;
      } else if (rewardsCount % 3 == 0 && rewardsCount != 12) {
        verticalEnd = verticalEndValues[(controllerCount - 1) % 3];
      } else {
        verticalEnd -= context.mqWidth * 0.24;
      }

      _fadeInAnimations.add(
        Tween<double>(begin: 0, end: 1).animate(
          CurvedAnimation(
            parent: controller,
            curve: const Interval(0, 0.3, curve: Curves.easeIn),
          ),
        ),
      );
      _moveAnimations.add(
        Tween<double>(begin: 0, end: horizontalEnd).animate(
          CurvedAnimation(
            parent: controller,
            curve: const Interval(0, 1, curve: Curves.easeInOut),
          ),
        ),
      );
      _rotationAnimations.add(
        Tween<double>(begin: 0, end: 1).animate(
          CurvedAnimation(
            parent: controller,
            curve: const Interval(0, 1),
          ),
        ),
      );
      _rewardMoveVertical.add(
        Tween<double>(begin: context.mqWidth / 2.6, end: verticalEnd).animate(
          CurvedAnimation(
            parent: _rewardSplitController,
            curve: const Interval(0, 1, curve: Curves.easeIn),
          ),
        ),
      );

      for (int i = 0; i < _rewardsControllers.length; i++) {
        AnimationStatusListener? listener;
        listener = (AnimationStatus status) async {
          if (status == AnimationStatus.completed) {
            if (i == _rewardsControllers.length - 1) {
              for (int j = 0; j < _rewardsControllers.length; j++) {
                if (_rewardsControllers.length % 3 == 0) {
                  horizontalEnd = j <= 2
                      ? 0.61
                      : j <= 5
                          ? 0.5
                          : 0.39;
                } else {
                  horizontalEnd = (j < 4)
                      ? 0.61
                      : (j < 8)
                          ? 0.5
                          : 0.39;
                }
                _moveAnimations[j] = Tween<double>(begin: 0.5, end: horizontalEnd).animate(
                  CurvedAnimation(
                    parent: _rewardsControllers[j],
                    curve: const Interval(0, 1, curve: Curves.easeInOut),
                  ),
                );
                _rewardsControllers[j]
                  ..reset()
                  ..forward();
                if (listener != null) {
                  _rewardsControllers[j].removeStatusListener(listener);
                }
              }
            }
          }
        };

        _rewardsControllers[i].addStatusListener(listener);
        _listeners.add(listener);
      }
    }

    _ribbonFadeInAnimation = Tween<double>(begin: 0, end: 1).animate(
      CurvedAnimation(
        parent: _ribbonController,
        curve: const Interval(0, 1, curve: Curves.easeIn),
      ),
    );

    _ribbonFadeOutAnimation = Tween<double>(begin: 1, end: 0).animate(
      CurvedAnimation(
        parent: _ribbonController,
        curve: const Interval(0, 1, curve: Curves.easeIn),
      ),
    );
    _buttonFadeInAnimation = Tween<double>(begin: 0, end: 1).animate(
      CurvedAnimation(
        parent: _buttonController,
        curve: const Interval(0, 1, curve: Curves.easeIn),
      ),
    );
  }

  Animation<double> _ribbonAnimate() {
    return _ribbonController.status == AnimationStatus.forward ? _ribbonFadeInAnimation : _ribbonFadeOutAnimation;
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        _buildAnimatedBuilder(),
        Positioned(
          top: 40,
          left: 60,
          right: 60,
          child: FadeTransition(
            opacity: _ribbonAnimate(),
            child: TextRibbon(text: _ribbonText),
          ),
        ),
        Positioned(
          bottom: 30,
          right: 0,
          left: 0,
          child: FadeTransition(
            opacity: _buttonFadeInAnimation,
            child: CustomCTA(
              text: widget.buttonText,
              onPressed: widget.onButtonPressed,
            ),
          ),
        ),
      ],
    );
  }

  @override
  void dispose() {
    for (final controller in _rewardsControllers) {
      controller.dispose();
    }
    for (int i = 0; i < _rewardsControllers.length; i++) {
      if (_listeners[i] != null) {
        _rewardsControllers[i].removeStatusListener(_listeners[i]!);
      }
    }
    _rewardSplitController.dispose();
    _ribbonController.dispose();
    super.dispose();
  }
}
