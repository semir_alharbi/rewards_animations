import 'package:flutter/material.dart';

@immutable
class RewardAnimationModel {
  const RewardAnimationModel({
    required this.assetPath,
    required this.ribbonText,
    required this.subtitle,
  });

  final String ribbonText;
  final String subtitle;
  final String assetPath;
}
