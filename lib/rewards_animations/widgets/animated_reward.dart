

import 'package:flutter/material.dart';
import 'package:rewards_animations/rewards_animations/widgets/reward_box.dart';

class AnimatedReward extends StatelessWidget {
  const AnimatedReward({
    Key? key,
    required this.animationFadeIn,
    required this.rewardAsset,
    required this.rewardTitle,
  }) : super(key: key);

  final Animation<double> animationFadeIn;
  final String rewardAsset;
  final String rewardTitle;

  @override
  Widget build(BuildContext context) {
    return AnimatedOpacity(
      opacity: animationFadeIn.value,
      duration: const Duration(seconds: 1),
      child: RewardBox(
        assetPath: rewardAsset,
        text: rewardTitle,
      ),
    );
  }
}
