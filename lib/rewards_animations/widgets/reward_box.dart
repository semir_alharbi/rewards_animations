import 'package:flutter/material.dart';

class RewardBox extends StatelessWidget {
  const RewardBox({
    super.key,
    required this.assetPath,
    required this.text,
    this.textStyle,
  });

  final String assetPath;
  final String text;
  final TextStyle? textStyle;

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 88,
      width: 88,
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(16),
      ),
      padding: const EdgeInsets.all(6),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Image.asset(
            assetPath,
            height: 40,
            width: 40,
          ),
          const SizedBox(height: 6),
          Text(
            text,
            style: textStyle ??
                const TextStyle(
                  color: Colors.black,
                  fontSize: 14,
                  fontWeight: FontWeight.w700,
                ),
          ),
        ],
      ),
    );
  }
}
