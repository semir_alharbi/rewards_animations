import 'package:flutter/material.dart';

class TextRibbon extends StatelessWidget {
  const TextRibbon({
    super.key,
    required this.text,
    this.height,
    this.fit = BoxFit.fill,
    this.margin,
    this.padding,
    this.textStyle,
  });

  final String text;
  final double? height;
  final BoxFit? fit;
  final EdgeInsets? margin;
  final EdgeInsets? padding;
  final TextStyle? textStyle;

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: margin ?? const EdgeInsets.symmetric(vertical: 24),
      padding: padding ?? const EdgeInsets.symmetric(vertical: 10),
      height: height ?? 38,
      decoration: BoxDecoration(
        image: DecorationImage(
          fit: fit,
          image: const AssetImage('assets/white_ribbon_label.webp'),
        ),
      ),
      child: Center(
        child: Text(
          text.toUpperCase(),
          textAlign: TextAlign.center,
          style: textStyle ??
              const TextStyle(
                color: Colors.black,
                fontSize: 14,
                fontWeight: FontWeight.w800,
              ),
        ),
      ),
    );
  }
}
