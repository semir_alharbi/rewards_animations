import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class CustomCTA extends StatelessWidget {
  const CustomCTA({
    Key? key,
    required this.text,
    required this.onPressed,
    this.backgroundColor,
    this.shadowColor,
    this.textColor,
    this.leading,
    this.textStyle,
    this.fillWidth = false,
    this.enabled = true,
  }) : super(key: key);
  final String text;
  final VoidCallback onPressed;
  final Color? backgroundColor;
  final Widget? leading;
  final Color? shadowColor;
  final Color? textColor;
  final TextStyle? textStyle;
  final bool fillWidth;
  final bool enabled;

  @override
  Widget build(BuildContext context) {
    if (fillWidth) {
      return _CustomCTA(
        onPressed: onPressed,
        backgroundColor: backgroundColor,
        shadowColor: shadowColor,
        leading: leading,
        text: text,
        textStyle: textStyle,
        textColor: textColor,
        enabled: enabled,
      );
    }
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        _CustomCTA(
          onPressed: onPressed,
          backgroundColor: backgroundColor,
          shadowColor: shadowColor,
          leading: leading,
          text: text,
          textStyle: textStyle,
          textColor: textColor,
          enabled: enabled,
        ),
      ],
    );
  }
}

class _CustomCTA extends StatelessWidget {
  const _CustomCTA({
    required this.onPressed,
    required this.backgroundColor,
    required this.shadowColor,
    required this.leading,
    required this.text,
    required this.textStyle,
    required this.textColor,
    required this.enabled,
  });

  final VoidCallback onPressed;
  final Color? backgroundColor;
  final Color? shadowColor;
  final Widget? leading;
  final String text;
  final TextStyle? textStyle;
  final Color? textColor;
  final bool enabled;

  @override
  Widget build(BuildContext context) {
    return InkWell(
      splashColor: Colors.transparent,
      highlightColor: Colors.transparent,
      onTap: enabled
          ? () {
              HapticFeedback.selectionClick();
              onPressed();
            }
          : null,
      child: Container(
        alignment: Alignment.center,
        padding: const EdgeInsets.symmetric(horizontal: 24, vertical: 10),
        decoration: ShapeDecoration(
          color: enabled ? backgroundColor ?? Colors.lightGreen : Colors.grey,
          shape: const StadiumBorder(),
          shadows: enabled
              ? [
                  BoxShadow(
                    offset: const Offset(0, 6),
                    color: shadowColor ?? Colors.green,
                  ),
                ]
              : null,
        ),
        child: Row(
          mainAxisSize: MainAxisSize.min,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            if (leading != null) ...[
              leading!,
              const SizedBox(
                width: 8,
              ),
            ],
            Text(
              text,
              maxLines: 1,
              style: enabled
                  ? textStyle ??
                      Theme.of(context).textTheme.labelLarge?.copyWith(
                            color: textColor ?? Colors.white,
                          )
                  : Theme.of(context).textTheme.labelLarge?.copyWith(
                        color: Colors.black87,
                      ),
            ),
          ],
        ),
      ),
    );
  }
}
